import React, { useState, useEffect } from 'react';
import axios from 'axios';
import "../Test/Pagination.css"

export default function Pagination() {
    const [page, setPage] = useState(1)
    const [halamans, setHalamans] = useState([])
    const [images, setImages] = useState([])
    const image = []
    const halaman = [];
    const limit = []
    const [limits, setLimits] = useState([])

    const getDatas = async (pages) => {
        try {
            const res = await axios.get(`http://zlzew.mocklab.io/pagination?page=` + pages);
            if (res.status === 200) {
                for (let index = 0; index < res.data.meta.limit; index++) {
                    image.push(res.data.data[index].img_url)
                }
                setImages(image)

                for (let index = 1; index <= res.data.meta.limit; index++) {
                    limit.push(index)
                }
                setLimits(limit)

                let total = (Math.ceil(res.data.meta.total / res.data.meta.limit))

                for (let index = 1; index <= total; index++) {
                    halaman.push(index)
                }
                setHalamans(halaman)
            }

        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getDatas(1);
    }, [])

    function click(pages) {
        getDatas(pages.number);
        setPage(pages.number)
    }

    const listItems = halamans.map((number) =>
        <a key={number} className="pagination__item is-active" onClick={() => click({ number })}>{number}</a>
    );

    const listImages = images.map((number) =>
        <div class="gallery__item">
            <img class="gallery__img" src={number} alt="" />
        </div>

    );

    const listLimits = limits.map((number) =>
        <div className="gallery__caption">IMG {page}.{number}</div>
    );

    return (
        <body>
            <section class="content">
                <div class="gallery">
                    {images.map((number) => {
                        return (
                            <div class="gallery__item">
                                <img class="gallery__img" src={number} alt="" />
                                {listLimits}
                            </div>
                        )
                    })}
                </div>
            </section>

            <section class="footer">
                <div class="footer__pagination">
                    <ul class="pagination">
                        <li>{listItems}</li>
                    </ul>
                </div>
            </section>
        </body>
    )
}
