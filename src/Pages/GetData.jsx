import axios from 'axios';
import React, { useState, useEffect } from 'react';

export default function GetData() {
    const [user, setUser] = useState([]);

    const getData = async () => {
        try {
            const response = await axios.get(
                "https://reqres.in/api/users"
            );
            setUser(response.data.data);
            console.log(response.data.data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getData();
    }, [])

    return (
        <div>
            <a href="/data">Back</a>
            {user.map(user => {
                return (
                    <div>
                        <p>{user.first_name} {user.last_name}</p>
                        <p>{user.email}</p>
                        <img src={user.avatar} alt="" />
                    </div>
                )
            })}
        </div>
    )
}
