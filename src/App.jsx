import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Login from "./Pages/Login";
import Data from "./Pages/Data";
import GetData from "./Pages/GetData";
import Test from "./Test/Pagination";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/"><Login /></Route>
          <Route path="/data"><Data /></Route>
          <Route path="/getdata"><GetData /></Route>
          <Route path="/test"><Test /></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;